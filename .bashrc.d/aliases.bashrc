# Bash aliases
alias ls='ls --color=auto'
alias la='ls -la --color=always | less -R'
alias ncmpcpp='exec alacritty --title ncmpcpp --class ncmpcpp --command ncmpcpp & disown'

# Pacman aliases
alias pac='sudo pacman -S' # Install a package
alias pacr='sudo pacman -Rns' # Uninstall a package along with dependencies and do not save configuration files
alias pacu='sudo pacman -Syu' # Sync repositories and upgrade the system
alias pacq='pacman -Qi' # List information about locally installed package
alias paclo='pacman -Qdt' # List orphaned packages
alias pacro='sudo pacman -Rns $(pacman -Qtdq)' # Remove all orphaned packages
alias pacex='sudo pacman -D --asexplicit' # Mark an installed package as explicitly installed
alias pacdiff='sudo pacdiff' # Always run pacdiff as root

# AUR helper aliases
alias apacu='yay -Syu' # Update package list and upgrade all currently installed repo and AUR.
alias apacac='yay -Sua' # Update all currently installed AUR packages.

# Add file to dotfiles git repo
alias dots='/usr/bin/git --git-dir=$HOME/projects/dotfiles --work-tree=$HOME'
# Get dotfiles from remote
alias dotget='/usr/bin/git clone --separate-git-dir=$HOME/projects/dotfiles git@github.com:ehs0329/dotfiles.git dotfiles-tmp'
# Sync from a temporary dotfiles directory to home
alias dotsync='/usr/bin/rsync --recursive --verbose --exclude .git dotfiles-tmp/ $HOME/'
