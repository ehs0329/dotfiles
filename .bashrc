#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Include files from .bashrc.d directory
for file in ~/.bashrc.d/*.bashrc
do
    if [ -f "$file" ]; then
        source "$file"
    fi
done

# Set solarized colors for the terminal because why not?
if [ "$TERM" = "linux" ]; then
    echo -en "\e]PB657b83" # S_base00
    echo -en "\e]PA586e75" # S_base01
    echo -en "\e]P0073642" # S_base02
    echo -en "\e]P62aa198" # S_cyan
    echo -en "\e]P8002b36" # S_base03
    echo -en "\e]P2859900" # S_green
    echo -en "\e]P5d33682" # S_magenta
    echo -en "\e]P1dc322f" # S_red
    echo -en "\e]PC839496" # S_base0
    echo -en "\e]PE93a1a1" # S_base1
    echo -en "\e]P9cb4b16" # S_orange
    echo -en "\e]P7eee8d5" # S_base2
    echo -en "\e]P4268bd2" # S_blue
    echo -en "\e]P3b58900" # S_yellow
    echo -en "\e]PFfdf6e3" # S_base3
    echo -en "\e]PD6c71c4" # S_violet
    clear
fi

# Environment variables
export EDITOR='nvim'
export PATH=$HOME/.cargo/bin:$PATH

# Bash prompt
PS1='[\u@\h \W]\$ '
eval `dircolors ~/dircolors-solarized/dircolors.ansi-universal`

powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
. /usr/lib/python3.11/site-packages/powerline/bindings/bash/powerline.sh
